# Tennis game
This is a coding kata making you cope with weird business rules, namely the tennis game points system.
See here for the full description => https://en.wikipedia.org/wiki/Tennis#Scoring

## Install requirements
```
. venv/bin/activate
pip install -r requirements.txt
```

## Unit test execution
```
. venv/bin/activate
pytest
```