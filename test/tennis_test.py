import unittest

from app.tennis import Game


class GameTest(unittest.TestCase):
    def setup_method(self, method):
        self.game = Game('A', 'B')

    def test_a_new_game_is_created_with_love_scores(self):
        assert self.game.score() == 'Love-All'

    def test_score_is_fifteen_love_after_player_one_wins_a_point(self):
        # given
        self.game.set_points(1, 0)

        # then
        assert self.game.score() == 'Fifteen-Love'

    def test_score_is_love_fifteen_after_player_two_wins_a_point(self):
        # given
        self.game.set_points(0, 1)

        # then
        assert self.game.score() == 'Love-Fifteen'

    def test_score_is_fifteen_all_after_both_players_win_a_point(self):
        # given
        self.game.set_points(1, 1)

        # then
        assert self.game.score() == 'Fifteen-All'

    def test_score_is_thirty_love_after_player_one_wins_two_points(self):
        # given
        self.game.set_points(2, 0)

        # then
        assert self.game.score() == 'Thirty-Love'

    def test_score_is_love_thirty_after_player_two_wins_two_points(self):
        # given
        self.game.set_points(0, 2)

        # then
        assert self.game.score() == 'Love-Thirty'

    def test_score_is_thirty_all_after_both_players_win_two_points(self):
        # given
        self.game.set_points(2, 2)

        # then
        assert self.game.score() == 'Thirty-All'

    def test_score_is_forty_thirty_when_player_one_scored_three_points_but_player_two_only_two(self):
        # given
        self.game.set_points(3, 2)

        # then
        assert self.game.score() == 'Forty-Thirty'

    def test_score_is_thirty_forty_when_player_one_scored_two_points_but_player_two_score_three(self):
        # given
        self.game.set_points(2, 3)

        # then
        assert self.game.score() == 'Thirty-Forty'

    def test_score_is_deuce_when_both_players_scored_three_points(self):
        # given
        self.game.set_points(3, 3)

        # then
        assert self.game.score() == 'Deuce'

    def test_player_one_wins_game_if_he_has_four_points_and_player_two_has_none(self):
        # given
        self.game.set_points(4, 0)

        # then
        assert self.game.score() == 'Win: A'

    def test_player_one_wins_game_if_he_has_four_points_and_player_two_has_one(self):
        # given
        self.game.set_points(4, 1)

        # then
        assert self.game.score() == 'Win: A'

    def test_player_one_wins_game_if_he_has_four_points_and_player_two_has_two(self):
        # given
        self.game.set_points(4, 2)

        # then
        assert self.game.score() == 'Win: A'

    def test_player_two_wins_game_if_he_has_four_points_and_player_one_has_none(self):
        # given
        self.game.set_points(0, 4)

        # then
        assert self.game.score() == 'Win: B'

    def test_player_two_wins_game_if_he_has_four_points_and_player_one_has_one(self):
        # given
        self.game.set_points(1, 4)

        # then
        assert self.game.score() == 'Win: B'

    def test_player_two_wins_game_if_he_has_four_points_and_player_one_has_two(self):
        # given
        self.game.set_points(2, 4)

        # then
        assert self.game.score() == 'Win: B'

    def test_player_one_has_advantage_if_he_has_four_points_and_player_two_has_three(self):
        # given
        self.game.set_points(4, 3)

        # then
        assert self.game.score() == 'Advantage: A'

    def test_player_two_has_advantage_if_he_has_four_points_and_player_one_has_three(self):
        # given
        self.game.set_points(3, 4)

        # then
        assert self.game.score() == 'Advantage: B'

    def test_player_one_wins_game_if_he_has_five_points_and_player_two_has_three(self):
        # given
        self.game.set_points(5, 3)

        # then
        assert self.game.score() == 'Win: A'

    def test_player_two_wins_game_if_he_has_five_points_and_player_one_has_three(self):
        # given
        self.game.set_points(3, 5)

        # then
        assert self.game.score() == 'Win: B'

    def test_player_one_loses_advantage_if_player_two_scores_four_points_too(self):
        # given
        self.game.set_points(4, 4)

        # then
        assert self.game.score() == 'Deuce'

    def test_player_one_has_advantage_if_he_scores_one_point_more_than_player_two_after_four_points_each(self):
        # given
        self.game.set_points(5, 4)

        # then
        assert self.game.score() == 'Advantage: A'
