from collections import OrderedDict


class Game:
    def __init__(self, player_one, player_two):
        self.literals = {0: 'Love', 1: 'Fifteen', 2: 'Thirty', 3: 'Forty'}
        self.player_one = player_one
        self.player_two = player_two
        self.scores = OrderedDict()
        self.scores[player_one] = 0
        self.scores[player_two] = 0

    def set_points(self, player_one_points, player_two_points):
        self.scores[self.player_one] = player_one_points
        self.scores[self.player_two] = player_two_points

    def score(self):
        if all(score >= 3 for score in list(self.scores.values())):
            if self._score_gap() == 1:
                return 'Advantage: ' + self._leader()
            if self._score_gap() == 2:
                return 'Win: ' + self._leader()

            return 'Deuce'

        if any(score == 4 for score in list(self.scores.values())):
            return 'Win: ' + self._leader()

        if self._score_gap() == 0:
            return self.literals[self.scores[self.player_one]] + '-All'

        return '-'.join([self.literals[score] for score in list(self.scores.values())])

    def _score_gap(self):
        return abs(self.scores[self.player_one] - self.scores[self.player_two])

    def _leader(self):
        if self.scores[self.player_one] > self.scores[self.player_two]:
            return self.player_one
        if self.scores[self.player_one] < self.scores[self.player_two]:
            return self.player_two
